@startuml
!includeurl https://oer-informatik.gitlab.io/uml/umlsequenz/plantuml/umlsequence.cfg
!pragma teoz true
mainframe sd OAuth2_RFC_6749 

actor "user \n:ResourceOwner" as resourceOwner

participant "     browser     \n:userAgent" as userAgent
participant "     webApp     \n:Client" as client

participant "      :AuthorisationServer              " as authServer
database "resource\n         :ResourceServer" as resourceServer


note over client
A <b>client</b> is an application making 
protected resource requests on behalf of 
the resource owner and with its 
authorization.  The term "client" does
not imply any particular implementation 
characteristics (e.g., whether the 
application executes on a server, a 
desktop, or other devices).
end note 


& note over resourceOwner
A <b>resource owner</b> is an entity capable 
of granting access to a protected 
resource. When the resource owner 
is a person, it is referred to as 
an end-user.
end note

& note over authServer
The <b>authorization server</b> 
is issuing access tokens to 
the client after successfully 
authenticating the resource 
owner and obtaining 
authorization.
end note

& note over resourceServer
The <b>resource server</b> is
hosting the protected 
resources, capable of 
accepting and responding 
to protected resource 
requests using access 
tokens.
end note

activate resourceOwner
resourceOwner ->> userAgent: http://my-webApp.org/showResource
activate userAgent
userAgent -> client: get(my-webApp.org/showResource)
deactivate resourceOwner

activate client
client -> resourceServer : getResource()
activate resourceServer
resourceServer -->> client : redirect(authURL)
deactivate resourceServer




note right of client
The client initiates the flow by directing the resource owner's
user-agent to the authorization endpoint.  The client includes
its client identifier, requested scope, local state, and a
redirection URI to which the authorization server will send the
user-agent back once access is granted (or denied).
end note



client -->> userAgent : redirect(authURL, clientURL, clientID, clientState)
deactivate client

userAgent -> authServer: getLoginPage(clientURL, clientState)
activate authServer
authServer -->> userAgent: showLoginPage()
deactivate authServer
userAgent -> resourceOwner : enter credentials
activate resourceOwner
note left of authServer
The authorization server authenticates the resource owner (via
the user-agent) and establishes whether the resource owner
grants or denies the client's access request.
end note
resourceOwner -->> userAgent: credentials
deactivate resourceOwner

userAgent ->> authServer : login(credentials)
activate authServer
deactivate userAgent

authServer ->> userAgent: redirect(clientURL, clientState, authorizationCode)
activate userAgent
deactivate authServer
note right of userAgent
Assuming the resource owner grants access, the authorization
server redirects the user-agent back to the client using the
redirection URI provided earlier (in the request or during
client registration).  The redirection URI includes an
authorization code and any local state provided by the client
earlier.
end note

userAgent ->> client : getPage(clientURL, clientState, authorizationCode)
activate client


client -> authServer: requestAccessToken(clientURL, authorizationCode)
activate authServer
note left of client
The client requests an access token from the 
authorization server's token endpoint by including
the authorization code received in the previous step.  
When making the request, the client authenticates 
with the authorization server.  The client includes 
the redirection URI used to obtain the authorization
code for verification.
end note
& note right of authServer
The authorization server authenticates the client, validates the
authorization code, and ensures that the redirection URI
received matches the URI used to redirect the client in
a previous step.  If valid, the authorization server responds back with
an access token and, optionally, a refresh token.
end note
authServer -->> client: accessToken
deactivate authServer

group loop ()

client -> resourceServer: getResource(accessToken)
activate resourceServer

ref over resourceServer, authServer : validate accessToken

alt AccessToken == valid

resourceServer -->> client: resource
...
else else

resourceServer -->> client: Invalid Token Error
deactivate resourceServer

client -> authServer: getNewToken(refreshToken)
activate authServer
authServer -->> client : accessToken
deactivate authServer

client -> resourceServer: getResource(accessToken)
activate resourceServer
resourceServer -->> client: resource
deactivate resourceServer
end



end

@enduml