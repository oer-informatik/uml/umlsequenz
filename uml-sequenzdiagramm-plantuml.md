## UML-Sequenzdiagramme mit PlantUML erstellen

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/uml-sequenzdiagramm-plantuml</span>

> **tl/dr;** _(ca. 10 min Lesezeit): Mithilfe des Tools plantUML lassen sich codebasiert UML-Sequenzdiagramme erstellen. In diesem Artikel werden einige Tipps und Tricks zum Umgang mit plantUML vorgestellt._

Dieser Artikel ist Teil einer Artikelreihe über UML-Diagramme: [Klassendiagramm](https://oer-informatik.de/uml-klassendiagramm) / [Sequenzdiagramm](https://oer-informatik.de/uml-sequenzdiagramm) / [Aktivitätsdiagramm](https://oer-informatik.de/uml-aktivitaetsdiagramm) / [Zustandsdiagramm (State)](https://oer-informatik.de/uml-zustandsdiagramm) / [Anwendungsfall-Diagramm (Use Case)](https://oer-informatik.de/uml-usecase). Für alle sind jeweils auch PlantUML-Anleitungen verlinkt.

Es gibt zudem noch eine Reihe Übungsaufgaben zu Sequenzdiagrammen: [Authentifizierung mit Passkeys](https://oer-informatik.de/uml-sequenzdiagramm-uebung-passkey) / [Authentifizierung mit OAuth2](https://oer-informatik.de/uml-sequenzdiagramm-uebung-oauth2),  [MQTT-Nachrichtenfluss](https://oer-informatik.de/uml-sequenzdiagramm-uebung-mqtt)
 sowie [kleinere Übungsaufgaben](https://oer-informatik.de/uml-sequenzdiagramm-uebungen)


PlantUML stellt einen einfach versionierbaren und änderbaren Weg zur Verfügung, mit OpenSource-Software UML-Diagramme zu erstellen. Diese kann auch direkt in die Dokumentationen der Repositories eingebunden werden (die Markdown-Engines von Gitlab, Github, Bitbucket & Co. unterstützen PlantUML nativ).  Leider nimmt es die  Referenzbeschreibung von PlantUML ([plantuml.com/de/sequence-diagram](http://plantuml.com/de/sequence-diagram)) mit dem UML-Standard nicht allzu genau.
Im Folgenden werden einige Tipps gegeben, wie sich mit PlantUML ansehnliche und relativ standardkonforme Sequenzdiagramme darstellen lassen.

![](plantuml/11_aufhuebschenHandwritten.png)

### Minimales Sequenzdiagramm mit PlantUML

PlantUML-Quelltexte beginnen mit `@startuml` und enden mit `@enduml`.
Die Kommunikationspartner werden automatisch erkannt, wenn man mit einem Pfeil signalisiert, von wo nach wo eine Nachricht versendet werden soll. Nach einem Doppelpunkt kann der Nachrichteninhalt übermittelt werden:

`Sender -> Empfänger: Nachricht`

Solange der stilisierte Pfeil auf den Empfänger zeigt können die Teilnehmer auch vertauscht werden:

`Eva <- Adam: Nochmal Hallo`

Codebeispiel: (
  [online bearbeitbar](https://www.planttext.com/?text=SoWkIImgAStDuN9CISnLqBLJS2rBj5BmICp9oUToICrB0Ka10000) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlsequenz/raw/main/plantuml/00_minimal.puml)
)

```plantuml
@startuml
Adam -> Eva: Hallo
@enduml
```

![Ein minimales Sequenzdiagramm](plantuml/00_minimal.png)

Die folgenden Diagramme wurden mit Layout-Befehlen angepasst (Details dazu im Abschnitt "Aufhübschen", siehe unten). Um die Diagramme UML-konform zu halten sind folgende Einstellungen im Kopf nötig:

```plantuml
@startuml
skinparam style strictuml
hide footbox
@enduml
```

- `hide footbox` versteckt die (in der UML nicht vorgesehenen) unteren Bezeichnungsboxen der Lebenslinien,

- `skinparam style strictuml` ersetzt die halboffenen Pfeilspitzen bei synchronen Nachrichten durch geschlossenen ausgefüllte - wie im Standard vorgesehen.


### Kommunikationspartner darstellen mit Plantuml

Sofern die Kommunikationspartner (Lebenslinien) ein besonderes Symbol erhalten sollen (in UML nicht so vorgesehen), einen Namen mit Freizeichen bekommen (z.B. wie es z.B. bei `objektinstanzname: Klassenname` der Fall ist) oder eine bestimmte Reihenfolge sichergestellt werden soll, werden die Kommunikationspartner vorab deklariert. Die Deklaration wird wie folgt notiert:

`SYMBOLTYP "NameImDiagramm" as ALIASNAME`

Codebeispiel: (
  [online bearbeitbar](https://www.planttext.com/?text=fPB1JiCm38RFpLDOUOoUjZ5ngYQamO6ZcxXRr3GkYQk24EzEsYAs8QqNSehiFz-lt_4c2KOPEgzIwqAF4JlOKuE3bqSESi2EG5U3SHRXY271yqTFOtH2KQkhDudU1Wg6u5D1FjiOUNnWpn7s6DkZHw6T7qWLMoCSTnmjnPK-clOKUGXsqOpdR9WwRhEWK4Nb6gfSe1NChsI5f8gddDqPrsEG5TQrwZxkwteS1nPQ9d6VEtV_y2ftEiihUS_ZBN2FWZMc32tM8jrtkw5bWISZZaFw3r2TdILecANcTwNwovoWYNbxADYReYudgrpLW0dgvQvm-d7GtIGCKBwal5fJVoae-vCSjAsXp7EWK6DePucjbRb40UrT4eBo4czMpLMroLxJddy3) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlsequenz/raw/main/plantuml/01_UebersichtKommunikationspartner.puml)
)

```plantuml
participant frontend
participant ":Backend" as backend
actor "martin :User" as martin
database "db :Datenbank" as db
collections "liste :Collection" as liste
```

![Unterschiedliche Symbole für Kommunikationspartner](plantuml/01_UebersichtKommunikationspartner.png)


### Synchrone Nachrichten und aktive Lebenslinien

Man unterscheidet aktive (als Rechteck gezeichnete) und passive (gestrichelte / als Linie gezeichnete) Lebenslinien. PlantUML löst dies durch die Befehle `activate LEBENSLINIENNAME` und `deactivate LEBENSLINIENNAME`. So lange eine Akteurin selbst etwas ausführt oder auf die Antwort einer anderen Teilnehmerin wartet ist sie aktiv.

Bei Nachrichten unterscheidet die UML zwischen synchronen und asynchronen Nachrichten. Bei Synchronen Nachrichten wartet die Absenderin darauf, dass die Empfängerin die Nachricht vollständig bearbeitet hat (z.B. die aufgerufene Methode beendet wurde). Es kann (muss aber nicht) eine Antwort auf eine synchrone Nachricht folgen (bei Methodenaufrufen: Rückgabewert). Notiert wird die Nachricht mit einem durchgezogenen Pfeil mit ausgefüllter Pfeilspitze:

`Absender -> Empfänger: SynchroneNachricht`

Die Antwort wird als gestrichelte Linie mit offener Pfeilspitze notiert, was in PlantUML folgendermaßen angegeben werden muss:

`Absender <<-- Empfänger: AntwortAufSynchroneNachricht`

Codebeispiel: (
  [online bearbeitbar](https://www.planttext.com/?text=hPHDJyCm38RFpQ-8UW4EE_1vc2QqZGaXRQgGneMZbtfhr3GkIKgP4F-Th6DhXzPA4-JIo7dzl8vhPMWT65TaYjbKwXmCP7o2IoYKUoJj8iYGXxD2o1Zu7BJbB-iSIoCTcf0rSYo-5QW5ya_6_HePG-K3AJ9y0YQTAt0uLWMoO2GScJ6P64t7EOXqPQZGyLPJ9jxGPfHwGS22cLpYJ6himEmb7IGMFFkxIo5pqAw3rLIruxwQxOZ8uROJxTpgz1Tl9bU9UqMbg3m4Ji310go71brNQdSxe7c1Gk4aQViN84jaZ7n9v1RqmLXUzuc7yyrSePagi1P33fRRROI1Dtxtb7s07sPcAFNKpnxnVlMfalT1nixiMel4a4OUWTz8aJZsm-Az-rhQzrDPJV1v-0HQemqlQlCQ6EEHu2akgn-Ne_HToc3GwnrDwlDTTVzIIRMBp6d5X9TNrpUtToqLD8AxNyQ6gEFDg_4D) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlsequenz/raw/main/plantuml/02_SynchroneNachrichten.puml)
)

```plantuml
activate starter

starter -> meinKonto: setName("Hannes")
activate meinKonto
deactivate meinKonto

starter -> meinKonto: getName()
activate meinKonto
starter <<-- meinKonto: getName(): "Hannes"
deactivate meinKonto

starter -> meinKonto: getKontoNr()
activate meinKonto
starter <<-- meinKonto: "123456"
deactivate meinKonto

deactivate starter
```

![Synchrone Nachrichten](plantuml/02_SynchroneNachrichten.png)


### Asynchrone Nachrichten

Bei Parallelisierung sind Nachrichten i.d.R. asynchron: die Absenderin wartet nicht darauf, dass eine Nachricht bearbeitet wurde und geht unmittelbar zur nächsten Aktion über. In der UML werden diese Nachrichten mit einer offenen Pfeilspitze notiert.

Asynchrone Nachrichten haben per Definition keine Antwortnachricht. Reaktionen auf asynchrone Nachrichten werden als neue selbständige asynchrone Nachrichten notiert.


Codebeispiel: (
  [online bearbeitbar](https://www.planttext.com/?text=fPCzJyCm48Rd-5TOcM3eq8nLLRMbOY5KIEt2U4ckZHN73kSB0I7-EsuA8K5j5hnOFh_lSn-obuw1k2wrS8Ko5H2KSeC7g3NVMyDRA566SPsg3EGEZ9FxzmeRKemKYfx7uKkD9aNv8QHVAoBRt5bjIMw0Yfq6nhMkKGIhb2sjBML88_UG5aUojSdEcYRt2Lrf2oy8H12h0yRAu0YcauoGHF3aUrUfgi3m2Aklkexxx8zZQndFaxZERM__y69rpFaPjRRD4BW1XWISXmPZBLtFDg1vWSQKbJNkFq2HgmpbmLfEx9iGrU-SPBWxlIka1mtEOIZ1INUE36MS4q8sbRD7L6RV7bfnsHsdbmpHY24I0dnlhxxGBkVtBYUBHHyzQ-Ctjx-6VjxBfcZC57Mc-NmoCAqHA475jJdA1D5ao2B3YuXU-AT-iVIEqo_z0W00) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlsequenz/raw/main/plantuml/03_AsynchroneNachrichten.puml)
)


```plantuml
starter ->> meinThread1:start()
activate meinThread1
starter ->> meinThread2:start()
activate meinThread2
starter <<- meinThread2:Bearbeitung beendet
deactivate meinThread2
```

![Asynchrone Nachricht](plantuml/03_AsynchroneNachrichten.png)


### Nachrichten ohne Absender / Empfänger

Sofern der Absender / Empfänger einer Nachricht für die betreffende Modellierung nicht relevant ist (oder unbekannt ist) spricht man von gefundenen (_FoundMessage_) bzw. verlorenen Nachrichten (_LostMessage)_. Sie starten bzw. enden nicht an Lebenslinien, sondern an einem _ausgefüllten_ Punkt. Ausgefüllt lässt sich dieser leider mit PlantUML nicht darstellen. Befehlsweise kann diese Notation genutzt werden (nicht ausgefüllter Punkt):

Codebeispiel: (
  [online bearbeitbar](https://www.planttext.com/?text=YyxNjLD8pibCpIinj598JqqhpKj9pKjLy4jCpYXApYZXWiefwDhbnofOAPIb5kNa5y5LP0AL0000) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlsequenz/raw/main/plantuml/04_gefundeneVerloreneNachrichten.puml)
)


```plantuml
'am besten geeignet, nur Ausfüllung fehlt
[o->> webserver: HttpGetRequest("www.beispiel.de")
webserver ->>o] : HttpResponse: "<html>...</html>"

'verloren nach / gefunden von links
[o->> webserver: HttpGetRequest("www.beispiel.de")
?<<-o webserver: HttpResponse: "<html>...</html>"

'verloren nach / gefunden von rechts
webserver o<<-? : Log aktiviert
webserver ->>o] : Zugriff loggen

```

![Gefundene und verlorene Nachrichten](plantuml/04_gefundeneVerloreneNachrichten.png)

Um zu signalsieren, dass die Nachrichten im _Nichts_ landen, wird in PlantUML an Stelle eines fehlenden Kommunikationspartners entweder das Ende des Diagramms angegeben ("`[`" bzw "`]`"), oder einfach das Ende des Pfeils (dessen Länge vom Nachrichtentext abhängt (mit `?`). In einigen Fällen werden die Kreise an den Pfeilenden nicht mehr komplett dargestellt, dann ist es ggf. erforderlich etwas mit dieser nicht ganz eingängigen Notation zu spielen. Beispiele, wie die Lösung aussehen kann finden sich oben.

### Objekterzeugung / Erzeugungsaufruf

Objekterzeugungsaufrufe werden in der UML notiert wie Antworten auf synchrone Nachrichten: gestrichelte Linie und offene Pfeispitze. In plantUML wird dieser Antwort  notiert  (`starter -->> meinKonto: new()`) und ein `create` vorangestellt:

Codebeispiel: (
  [online bearbeitbar](https://www.planttext.com/?text=fPBFJiCm3CRFpLDOFS5XBx33j8s9owe9QLmuUgctHKtZuhWKX7XtiWxj3sYza4kapvz_dsnv6XL5szgRMBdGe60D2zfYw_MHWwwm9iYBrheIOOqXmlD7Gvqu9SdDHK-arvQ29VWqaDvCXBi7zYomGAdM7fNclYMJpQoop5bAae4wscedt8RowEdsAJ3MN2L3PhB2RQbmWGOm9yi0oMHFQNPdNOD11rYNhjkuhyjrh5Zfk8dRtBxwYrUutLvVo7lkhe4BLDnWJD1iQAJRQLUqPF1arN68_m6QlIi9jiowuNTZclEU85yVxefawJ56oW4ZnADYC0M_9Sf9k6wioOLbEZs6SV_rlIVHMA53xrdueS1eD9cSvJ44wkxkZIdfRzwKGdau-My0) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlsequenz/raw/main/plantuml/05_Objekterzeugung.puml)
)

```plantuml
@startuml
participant "StarterKlasse" as starter
activate starter

participant "meinKonto :Konto" as meinKonto
create meinKonto
starter -->> meinKonto: new()

deactivate starter
@enduml
```

![Objekterzeugungsnachrichten](plantuml/05_Objekterzeugung.png)


### Objektdekonstruktion

Dekonstruktionsnachrichten sind normale (synchrone oder asynchrone) Nachrichten, die das aufgerufene Objekt zerstören (je nach Programmiersprache: den Dekonstruktor aufrufen, Referenz auf NULL legen...). Die Zerstörung wird notiert, in dem an das Ende der Lebenslinie ein "X" geschrieben wird. In PlantUML wird `destroy meinKonto` notiert:

Codebeispiel: (
  [online bearbeitbar](https://www.planttext.com/?text=fPDDRi8m48NNyIciI9L0WYeLErGXeAWRVbI9RheSaW6iE9xKTaXfrKtFqrFr97MWIaWBsJGRIy_lVSyUEKDZGTiiaSp4GgMW8U4JN4CcxJqfku04kJ_BGX41Nu8o_76VOgw5HUspaup1vmnLYFoDSVUDjARyZYHfFW4TBoLO7CiCcJSABUanwGXrmpw4yKPJfgAZ9z-wGfDGx0mUysPYZJEXi05JMXf8p7jmTnUXI47P1jQfwpBk_NGS2x9udCHbxc7t5sycDblxX59INWTEmC8AZ8DwJLUwt5QZEOF4q0fIvZz0jXKHyZMHNT4BOsar9-ulYtU5UYh16FGv66wE2WDNl7EKKgW74nHgwfuUyVvXEMHBaOKQYsmb_51ujpiOL7AVAynRDq5m7GHV7vyTxiypusm9ADNXyw9G-UqgN8lkKByAPR3LksfNn_rRUNlRxPxDjVizLyeYR0ZNA-DY6MLhIMXGjSxMdiL5QAoc_NbREU4XggZuoRy1)
)

```plantuml
starter -> meinKonto: kontoAufloesen()
activate meinKonto
deactivate meinKonto
destroy meinKonto
```

![Objektzerstörung](plantuml/06_Objektdekonstruktion.png)


### Selbstnachrichten

Nachrichten kann jeder Teilnehmer auch an sich selbst schicken (Methodenaufrufe eines Objekts auf sich selbst). Absenderin und Empfängerin sind dann identisch.

Codebeispiel: (
  [online bearbeitbar](https://www.planttext.com/?text=fPCzRuCm48Pd_2jEJ8bKXdujKHGbAUeI54LAbuuEN8Y5iQa_IZ_K_rvZqeP4XQKC6D-zx-FpOS-reShOaXDTC55HHKk8SKyjDuzIc3Kj4SB4fYoZiAL2mzDxXRLY1bL8EXwDBnP5Yl19m3qBfMJz8BbK45DLR3arkEGMIR18ZLHBgJ9K0tcQ5hcILcIjfZwu1NKf2oS8I92mFIPCu03cLp908i76xPsbhAB23B2wgdxSLxSTQscmxKG_rsSlU0dB3-OPEPVrEJ2cXkweTj1WQ4lzgvtHd81ZQfWK-Zz0Sc0PmbvAivDlXD3crq6uGNSuOB9oF0o1QgYQG9Dbhyvyd5QdfaBeGthb3fnqncRmngBv8iHd8PgrSvYquyBg3vkFnfEjKKpa9_xHvOTezkF9qNYmE-yY6uqlvVuzdKPHZtvoVNDxTNTFChnSnrVNTPvN5yQkzXC9c3RGii8_O9r8soOoHv4rr_CR) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlsequenz/raw/main/plantuml/07_Selbstaufruf.puml)
)

```plantumlquelltext
konto->konto : getKontostand()
activate konto
konto<<--konto : getKontostand():123,45
deactivate konto
```

![Selbstaufruf](plantuml/07_Selbstaufruf.png)

### Schleifen im Sequenzdiagramm

Schleifen werden im UML-Sequenzdiagramm über kombinierte Fragmente dargestellt. Im Fall einer Wiederholung über das Schlüsselwort `loop` in Kombination mit einem _guard_ (im einfachsten Fall der Anzahl der Schleifendurchläufe oder einer Bedingung, bis zu deren zutreffen der Schleifenrumpf ausgeführt wird).

Codebeispiel: (
  [online bearbeitbar](https://www.planttext.com/?text=fPJ1RjD048RFtgSOvKAZaXUm2cfAL0H45L8H4iUnFR5NtkoQtN428Rm9ZyAjByROJbh79POGljZxU_QRVsR7lWwCdkkrKQ7KjaAFQrZG2cl3jyxo7Qu99iiwrHd2FTe0dxvNjFMQoKzKRq-WhpNPbE270hdcthlj6sUSXmNwyjuWqusfIKNpb9s_SJuZF_8UqpBthhPP5xCj965Okr828XKjzOgMsj88vZ5aXAIYZrAxJdM5baTO_QZpk9_zTjmvfguJvxdjsm5lgVE2lv0nRdiAN21ZWa6WqLX9vxETq2J0KChQsV0_G5NeZ63b72VkcrBO71rCudTsGq6tT4zfOMLK00FeHkQ3qWJhZR06SlNKQvXK96CMFiWSEeZRMmigcoUbJZR2x7MdGmm9TU9dyZjjWzJojkO24x8NcqwgRHxUO-5f6g-CGtvoSs0txQZ0E5V1yprglz-3UW6t3xzp1f8fip1Fv4EoMRE2NUt5GmXYmyB3Bx4JT5gmwlmTZSPo_Fd15xo2GnQwE6eleLaZxwP3TntdwcesEvB-nfdsvCkMfJ8QaAGGHJORzh917vlvRzryJFayLqyuFN9rBHQQFz0V) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlsequenz/raw/main/plantuml/08_Wiederholungsstruktur.puml)
)

```plantuml
group loop (verzinsungsJahre)

konto -> konto:   guthaben = verzinse(guthaben, zinsatz)
activate konto
konto <<-- konto: guthaben = verzinse(): guthaben + zins
deactivate konto

end
```

![Wiederholungsstruktur](plantuml/08_Wiederholungsstruktur.png)

### Bedingte Anweisungen im Sequenzdiagramm

Auch bedingte Anweisungen werden als kombiniertes Fragment dargestellt. Der _guard_ (die Bedingung) wird von PlantUML mit eckigen Klammern umgeben, so dass die Notation einfach wie folgt ist:

```plantuml
alt BEDINGUNG
else else
end
```

Das zweite `else` taucht als _guard_ im Diagramm auf. Falls ein `elseif`-Block folgen soll würde `else BEDINGUNG` notiert.


Codebeispiel: (
  [online bearbeitbar](https://www.planttext.com/?text=fPFBJiCm44NNyLUCke95-m5LYreeR2WLKjcmd2QJn8hhAVQu1I7-7ISfq8UQ3LxOaUVUCmyv4o_e9AoDyfMs6tIuXXdb68myi9K5hWbwyv3g3669riFBnuPsJWkvdZhmU7eBP5E2JmLnJPtZtHqRTZ13LoqD2jsQG2gPfiBkbbr6hYEEQLKu3ZPhDRio9lHhhg8WKSbSvpJNbZemlv8EaagUO-ywrHkqqi4wL5t6VHsEOy52xIGkSvle2M-kYr9UoHZU7GDdABX27w59LqkNintHei1GAfgj_mzGbJeZo9bbnUzAOIHkOnWU8ufKiqF_9ZPfgo5KyJanEQOyjy4MJqo5qMbvrSYk_yorMg4HM55ycpRpLItOos0y1d7X1puQzVjxV7Cx60mK6KzGRqhLaRg0PLg678UWlIpPRCdAMTPMqZeQQdi1G_3DnxqhQ6MrLncTUivAkUneUckAj5aSdvh4i_xtlW40) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlsequenz/raw/main/plantuml/09_BedingteAnweisung.puml)
)

```plantuml
alt bestandskunde(Kunde) == true

Kunde <<-- Bank: true

else else

Bank -> Schufa: istSolvent(Kunde)
activate Schufa
Bank <<-- Schufa : schufaErgebnis
deactivate Schufa
Kunde <<-- Bank: schufaErgebnis
deactivate Bank

end
```

![Bedingte Anweisung](plantuml/09_BedingteAnweisung.png)

### Concurrency - Parallelisierung im Sequenzdiagramm

Wenn mehrere Interaktionen parallel verlaufen wird dies mit dem kombinierten Fragment `par` notiert.

```plantuml
par
INTERAKTION
else
PARALLELEINTERAKTION
end
```

Codebeispiel: (
  [online bearbeitbar](https://www.planttext.com/?text=fPFHIiCm58QlUvuYz4elvWEC8TiSNfKXJ10lJvlJDZHDvacwAkAxUtIuTUAAi5mb_7--_-GacOU87BlMGcYCso9ZgrPKOcVZlNTnZIsfDEiAer5jq0Nr-BQbdaqaJc6m9z1BHwuWzGvAneBPztVUUbOhv6PZCTBITWJ9eeYUbvuryOYEHLEnxvpUU_fQ0aFh6p4aa6IcfCmu6i4SB2CaI1xaxAOmMtHnX3LqdSTz3Dknzf7sdJZF_LP_yJ9JrV6PhFNzAN254NCC0at6ZdG-xOGc1aj5DDw5Iu1G6qsgz3xc_XK0XRWJMItbyWEn0f0u-5bDRjKJvJBR4K-LHKrNQMsSlAIA9JitLjzKfaolZw23_m2PpIQJVsAcfR44clwW0Tb0euL5QO6zfAHVW95Yo6aOAiVkmLoqhx_v2G00) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlsequenz/raw/main/plantuml/10_Parallelitaet.puml)
)

```plantuml
activate Browser

par
Browser -> Webserver: lade("hintergrundbild.gif")
activate Webserver
Browser <<-- Webserver: lade("hintergrundbild.gif"):file
deactivate Webserver

else
Browser -> Webserver: lade("titelbild.gif")
activate Webserver
Browser <<-- Webserver: lade("titelbild.gif"):file
deactivate Webserver
end

deactivate Browser
```

![Parallelität / Concurrency](plantuml/10_Parallelitaet.png)


### Referenzen: Auslagern von Abschnitten in weitere Sequenzdiagramme

Zur Übersichtlichkeit können Sequenzdiagrammme aufgeteilt und ineinander referenziert werden. In PlantUML wird dies über das Schlüsselwort `ref` umgesetzt, wobei die zu überdeckenden Lebenslinien als kommagetrennte Liste angefügt werden.

```plantuml
ref over nameLebenslinie1, nameLebenslinie2 : nameDesReferenziertenSequenzdiagramms
```

Codebeispiel: (
  [online bearbeitbar](https://www.planttext.com/?text=fPDDJy90443FdL-nuQI97BWsn80GJmHDyE9ns0wmThjJTwUW4V-x2mLiJUZ5dYOpRzzy98LH4FHI5HP2Rbo97WirfHLMLXxPoHmBKhrPfKs6Qe4kg9VFadRU2FaUDDu4Ug_8QL9VeE8tzfvt4xRirHHzlh0ez60hWcIiXVq3-unyHnrrllPSkQncTflOC1ISHo21P6PMD3EEEZGNfCC4oNFStMXJefCELvEwhljkdcFEGlKbhdkFrJ--cLblv9MivLrRE4N19OOeJRfMkjwjPOk09Is6NVYF43Oc8xLYbYL_09I_Tr8zRGqv6QXqSWnw2eCwvTfWg9PlSHQLBkgW9aCRucKWloL_yB7JdeIUJgcQvm40nessSRnB7pWFqR-FnZIGB2JMRm8NT8nk6sy2U5efZiQ3xIucLAhq1jswPY-ouR3VF-YsR3B8g6c0_NuFhTnvaX6vBFvbFm00) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlsequenz/raw/main/plantuml/13_Referenz.puml)
)

```plantuml
activate client1
client1 -> cs:setState(someState)
activate cs

ref over co1, cs : changeState

client1 <<-- cs:void
deactivate cs
deactivate client1
```

![Referenzen auf andere Diagramme](plantuml/13_Referenz.png)

Der referenzierte Bereich wird dann an anderer Stelle in einem Interaktionsrahmen als eigenes Diagramm angegeben. PlantUML nutzt hierfür das Schlüsselwort `mainframe`. Der UML-Standard erwartet, dass dem Namen des Interaktionsrahmens  `sd` (für _sequence diagram_) vorangestellt wird. Beispiel:

```plantuml
mainframe sd nameDesDiagramms
```
Codebeispiel: (
  [online bearbeitbar](https://www.planttext.com/?text=fPEnRi8m48Rd-2ciJEt0m1hH2WhgX6YbTEbuiI_WalXI-m95LT-z9e52G6Hff4YMx_Ft-qw967i6ntLP2BysjW87fPnX3dN1psHv0INAU5uheq6cOBrysrMuTORHnUBiZCVF6gr2-IraU2REqNPA1Jav0xTE2s1yAceKqKGnkITo6brF7THwwQYskcMsgn3eIre781BHtEGuDnPxD7z8Zqb4hw5teqm5bdjSvzHjtS_vE1R4s4xYjhUfNlZcPhdYTomAsdQ5Cs38m0TfrDVIxRIEBG05AZPa_N-4OcKqofo8C_eIeWHZSxV_OhoMQWLsYIa7N8ZgD44P-phx2E4oITj5BC5BvRiGPHxT1jrG9bEooY7Zos6hvMae18GMDY5WVrhvmQFoYIKs-UxklbCJRPM6ILtfiDSf1r5ucyF7qERM3qlaPd4X2_1eD1XSuPxAGxiQknThyBFmP4D6TwXzCqVmrCCrvyKOhGv_woy0) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlsequenz/raw/main/plantuml/13_ReferenzierterInteraktionsrahmen.puml)
)

```plantuml
mainframe sd changeState

participant "subject :Subject" as cs
participant "observer1 :ConcreteObserver" as co1

activate cs
|||
deactivate cs
```

![Interaktionsrahmen](plantuml/13_ReferenzierterInteraktionsrahmen.png)

### Weitere kombinierte Fragementen

UML-Sequenzdiagramme sehen eine Reihe weiterer kombinierter Fragmente vor, die in Plantuml über die folgende Notation umgesetzt werden:

```plantuml
group opt
  client-> server:request()
end
```

Neben dem hier gewählten opt können beliebige ander Zeichenketten genannt werden (strict, assert, critical wären z.B. in der UML noch vorgesehen).

### Zeitliche Zusicherungen

Sofern die Abfolge von Nachrichten an bestimmte zeitliche Zusicherungen gebunden ist, können dies im Diagramm angegeben werden.
PlantUML nutzt dafür _Label_, die in geschweiften Klammern angegeben un den Nachrichten vorangestellt werden.

Zeitdauern zwischen Label können dann unter der jeweiligen Zusicherung, die als _guard_ nach UML-Notation in geschweiften Klammer stehen muss, angegeben werden. Da dies nur mit der neuen PlantUML-Engine `teoz` dargestellt werden kann, muss diese derzeit noch vorneweg über den Befehl `!pragma teoz true` aktiviert werden:

```plantuml
!pragma teoz true
{ChangeStateStartLabel} client1 -> cs:setState(someState)
activate cs
{ChangeStateStopLabel} client1 <<-- cs:void
deactivate cs

{ChangeStateStartLabel} <-> {ChangeStateStopLabel} : {500ms}
```

Codebeispiel: (
  [online bearbeitbar](https://www.planttext.com/?text=fPJ1Rfj048RFx5DiVKeEbk93ByYDajZg2QMLwAN7OHdZhPSTkZlOJQsyUnS83ZY2Iv4iuPrllvdzGJnu1iTrQSIdoa5HWcIalv9TZKBuWxOLE2Zb5dTG6_vAbf-XHBb8QgLpa2bOBt-yL7Xocj4jXdiy_gxHAfHd8SFrw1oTDcJ8oIsuGsg0ySc4Dj6ZOd9Fv79qCtLGXy9HRVEEEUr3Gr_I8G2HY1AzmqHRdD5Sa1cJYBw7EBJI5LYUSGsfQTthC8vdOkoIcFQsrIjVeeiz_qHZw3GMRe4X0n-aqToHfhkDR04mg5YJzVyZ57kTezmHSKP_XAZUSv8BPJHQNibuqzui97ZvjZO6VPtz2hF8EErkEjAF8SeykYEwnaTMEMJyzhRKyRGI0iA9Zc6yInzntkp15fXoM4sRTp-13Crh3yZbVUWKU-IMkF5KTkpjKEQ5yilxm5bYlNkvkHtLH5UbLLnNUICPbiDKuTTkxa_G-hyKVSknBC3hzNBv0RyC9d8S3zRYW-RnaNG-effaUl3z31yvVvKMLLTXDOOchdQdc0vt7LATKCNo_FdkhlJDktFLVFYlVtmFQFFcQoN-0G00) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlsequenz/raw/main/plantuml/14_ZeitlicheZusicherungen.puml)
)


![Zeitliche Zusicherungen](plantuml/14_ZeitlicheZusicherungen.png)


### plantUML-Webservice zur Diagrammerzeugung nutzen

PlantUML bietet einen Webservice, der Diagramme unmittelbar online rendert.

* Variante 1: **Codierter Quelltext**: PlantUML kodiert den Quelltext, um ihn so über den
Webservice zugänglich (und bearbeitbar) zu machen. Die URL ist in diesem

  Fall wie folgt aufgebaut:

    [http://www.plantuml.com/plantuml/AUSGABEFORMAT/CODIERTERQUELLTEXT](http://www.plantuml.com/plantuml/uml/fPA_JWCn3CPdyXHM5yfmBr09gVy40rMNWX28nStvxg9BdCfnE07YtScfAdHWWcon_VtysSayAOhcu4tg7HzGCC2Q6inURoBh5WF1P9Ejgn5so0dktmuqY5EIYJdJF2HQOQ8F0-KiezGag-YZm1gbttbKMlfCnopQlfMOkJvM35sXfH1xCfzdn6tKF-4shktqYRoFG-6T0HTMe_pRu3bG90w_GSpbBJ59yG3lES264Z6yHXwtL8rhgjOEsu889KwE6xGToSnuQXGqWemZGEs4hBh8XRVebR8uXeQIUcgBpk0u3ypkYa_7Cy04DYUDWQG8JWy2DJMENJ73C0rEuPcS9yvXBzbsyCBNMngyOxeoEP4T5TD752ePs9TUPGRYgn7-VJFcr0UgwYTy0SRCYUlobxu0)

    wobei als *AUSGABEFORMAT* `png`, `svg`, `eps`, `epstext` und `txt` genutzt werden kann. Wird als *AUSGABEFORMAT* `uml` gewählt erhält man den bearbeitbaren Quelltext.

* Editierbare Links lassen sich auch über den Service von planttext.com erstellen, sie nutzen die selbe Quelltextcodierung und URLs nach dem Muster:
 [https://www.planttext.com/?text=CODIERTERQUELLTEXT](https://www.planttext.com/?text=bPBFJW8n4CRlVOg9Bs2ywhe199u8CS6OU2pjiDjijqEcKpQ4y6RUV35Ry028YVOuVxxVV5ywYg9PKkzLx5nOQzOzJ76bavTd2ZBNFSBDB1bdDInqYF2wNUF0Jf1lrCdEs8ZREDdk5EGtqQPhc5AmJ-I98GOQZWrYYtmiJZLt2wy59pxXeJjrkgTWBxURbg8CRMQUJVqgfVOdXyr9SFS7zYLqvffMtj7xVFd-p2ap3LUnwf2bkb-ODWSaSFS0AcGySD620wQgF1djNnXDzk34KQZhRriO4Tw8bsXTQ59ee4ynGhMiDyJLxR8-AenJN7r-j5m6RDbX67T51v1pmtk1Y2wen_pEa3d4wyovDkrFQCZLGlqN58E5OZb7GMi5EPDHBfNlzGK0)
 Auch in den erzeugten PNG-Dateien wird der codierte Quelltext als Titel hinterlegt, so dass sie sich relativ einfach später weiterverarbeiten lassen.

- Variante 2: **Quelltext direkt online rendern**: der PlantUML-Quelltext ist online als Resource verfügbar und
soll gerendert ausgegeben werden. Hierzu muss eine URL nach dem Muster:

    [http://www.plantuml.com/plantuml/proxy?fmt=AUSGABEFORMAT&src=https://URL\_PLANTUMLSOURCE](http://www.plantuml.com/plantuml/proxy?fmt=epstxt&src=https://raw.githubusercontent.com/hannsens/plantUML-UseCase-InfoSheet/master/plantuml/01_Bestandteile_UseCase.plantuml)

    erstellt werden, wobei als *AUSGABEFORMAT* `png`, `svg`, `eps`, `epstext` und `txt` genutzt werden kann. Diese Funktion scheint derzeit deaktiviert zu sein oder nur Entwicklern vorbehalten.

### plantUML-Formatierung: Aufhübschen von Sequenzdiagrammen

Über Skins bietet PlantUML die Möglichkeit die Diagramme in Schriftart, Farben und Aussehen individuell anzupassen. An drei Beispielen soll das hier kurz gezeigt werden, wie die Konfiguration mit `skinparam` das Aussehen der Sequenzdiagramme individualisieren kann.

#### Darstellung ohne zusätzliche `skinparam`-Befehle

Ein Auszug aus dem Observer-Pattern sieht ohne Anpassungen des Skins mit PlanUML folgendermaßen aus:

Beispiel: (
  [online bearbeitbar](https://www.planttext.com/?text=TP9DReCm48NtSmehArWWbIrAeaWzG2Fb18wPHAw8ZJm3KczV_s02jRe0ZJsVt_CRk11BnzDh063D0uglEw6RqOchP4Pd04OFQAL7QLWSrA3HS2kwprWSX2InxEr1ckxVg5XqjrGaalQGNSo2dZNA8MFsJxnj0QHYFKl6rUUzuTl-4StPAtUox_FXQXMkDpH5-7Xicb3tkEriLy7qwUmqYi7QKLJjHmsGWoSlGhwnXokoBunL3NidQiwUCvRruwVQtm9IrxRTDFP1f2uYnSCvHDG_FRDb4IaaAl3rOd-2AehunhoRhUwB0MJmdQ7aQ9tACjI401h_-d--osU4Y-VYd_SB) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlsequenz/raw/main/plantuml/11_aufhuebschenOhne.puml) )

![Beispiel ohne Formatierung](plantuml/11_aufhuebschenOhne.png)

#### Skin mit angepassten Farben und näher am UML-Standard

Im folgenden Beispiel (das für die Diagramme oben verwendet wurde) wurde kosmetisch nur Farben und Schriftart anpasst, aber zusätzlich auch die oben genannten Tipps verwendet, um das Diagramm UML-konformer erscheinen zu lassen: `skinparam style strictuml` für die Pfeile und `hide footbox` für die Enden der Lebenslinien.

Codebeispiel: (
  [online bearbeitbar](https://www.planttext.com/?text=fPHFRwCm4CNFcKzXvPGS8cskg5lrJzHJrAsKlVJes9F46-EXzf1iLFMxrq0W41GkHG8PviTxCm-BXy3ISvvQ27lZCkbbAWAVBCQhD-ggii2Dp2s_aEDNcQ8OBtDbj1GhwOBuUyhmw0sZ7qDR3JzoT0h59uXuF7fFnsUovCL2-ltAIiOdcoEC7XMJVoAlqG_KfTflFULELynn5mr3Idi462tD1fV6uO18WmpflCL8Z3APT3mWrQPkgtsritWbnYg6sxfbzKflQROxVaThwTWLN4YMQncYw6YWXz5jjuvQ12mgDkJ2JmHtHgFO4F6Q_WEmuRYR_gm3-WDwyIOvxX07a5tY4sDb3JgUY-IvN8o53EByh0k6VFqljYYILRMeoD25w6nMw95J7Xbh_ugdEO2CWnvYrutFvK4inrFCxgDo8hMkNvuqmjCM7KhuxcusAzOQsvNsNM5Qv9a9IvI9oVpN5A0UlF8Ao2kEy2HGYkLg2bsdCBkFd2CsczEasmLKLPedUQOBaMblfFBbUeXI__UsjkoDL4pKmvl6hWPLeSHRvic1ZEu5K8EN6VfSQ58vXreXW2vURkTx_epm4BdYjmNV)
)

```plantuml
@startuml
skinparam style strictuml
skinparam DefaultFontName "Lucida Sans Typewriter"

skinparam sequence {
    ArrowColor DarkSlateBlue
	ActorBorderColor DarkSlateBlue
	ActorBackgroundColor whitesmoke

	LifeLineBorderColor DarkSlateBlue
	LifeLineBackgroundColor whitesmoke

	ParticipantBorderColor DarkSlateBlue
	ParticipantBackgroundColor whitesmoke
}

skinparam Note{
    BorderColor DarkSlateBlue
    BackgroundColor LightYellow
}

skinparam Database{
	BackgroundColor whitesmoke
	BorderColor DarkSlateBlue
}

skinparam Collections{
	BackgroundColor whitesmoke
	BorderColor DarkSlateBlue
}

hide footbox
'[...]
@enduml
```

![Für diese Seite genutzte Formatierung](plantuml/11_aufhuebschenSkin.png)

#### Sequenzdiagramme per Formatierung als _Entwurf_ kennzeichnen

Um den Entwurfscharakter eines Sequenzdiagramms zu unterstützen kann ein Layout verwendet werden, dass Handschrift ähnelt. Besonders wirkungsvoll ist das, wenn auch die Schrift authentisch handschriftlich aussieht. Dies ist z.B. bei der Schriftart "FG Virgil" der Fall. Welche Schriften das jeweilige System bietet lässt sich mit dem PlantUML-Befehl `listfonts` anzeigen. Wichtig ist: sofern eine Vektorgrafik ausgegeben wird (svg, eps) muss die Schrift auf allen anzeigenden Computern vorhanden sein, andernfalls werden Ersatzschriftarten verwendet.

Beispiel: (
  [online bearbeitbar](https://www.planttext.com/?text=TPFFReCm3CRlUOge9pWWZIlggcgRjjisgK_WWe4CY57YjElRB_mj5EqGPEAV_ULpbADbCEoQMjXAwHOCDF8DSt0rlvFcJsXGxbu0gyPLyiFLgjajo19qTZMA6RLaut2HQaXJMXhozG-P5j8A2ZpnhKPfCEL5pfQGqLNfGkPGMnIbob3cH7ocNo5OiI_vEbiq5pJo6xomqKBuMbQfQa6ptAMrGisnJ5xxO2V1od5l3Lft_l7gCZaDmK3QDKIZMDUFT6gGST8VU8g5W9JL1HXddVk6J_ibeuFld42MJSN1t3XSqBQ7z_iewk8CbvdbNoTQ67Ajh8bQ6SHFeH2JyK7B8f_Omu7rLz17eLWhsUZWEKsiybkmFeKOiXGdhisw9k76KbyycUZxFnUJvCPIvsY3pmTxC9hQ7b-89nTIsMO04tZtiENiF9Lng0CYKFlF__CThr4SFTUz3V47) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlsequenz/raw/main/plantuml/11_aufhuebschenHandwritten.puml)
)

```
skinparam style strictuml
skinparam DefaultFontName "FG Virgil"
skinparam handwritten true
skinparam monochrome true
skinparam packageStyle rect
skinparam shadowing false
```

![Entwurfscharakter durch Handschriftart hervorherben](plantuml/11_aufhuebschenHandwritten.png)


#### Diagramme vereinheitlichen und Formatierung auslagern

Damit nicht alle PlantUML-Diagramme die selben `skinparam`-Sequenzen am Beginn der  plantUML-Datei nennen müssen, kann eine zentrale Konfigurationsdatei eingebunden werden, die die Formatierungen enthält:

```plantuml
!includeurl https://PATH.TO/MY/UMLSEQUENCE_CONFIG.cfg
```

Alle Befehle in dieser Datei werden ausgeführt, als stünden sie in der umgebenden PlantUML-Datei. Um ein einheitliches Layout zu erreichen ist diese Funktion sehr praktisch!

### Links und weitere Informationen

Es finden sich zahlreiche Quellen und Dokumentationen zu PlantUML im Netz, erwähnenswert sind u.a.:

- [Zuvorderst natürlich die offizielle Dokumentation von plantuml](https://plantuml.com/de/sequence-diagram)

- [Die PlantUML-Sprachreferenz als PDF](http://pdf.plantuml.net/PlantUML_Language_Reference_Guide_de.pdf)

- [diese Übersicht aller Skinparams ](https://plantuml-documentation.readthedocs.io/en/latest/diagrams/sequence.html)

