## Nachrichtenfluss eines MQTT-Projekts (UML-Sequenzdiagramm Übungsaufgabe)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/uml-sequenzdiagramm-uebung-mqtt</span>

> **tl/dr;** _(ca. 20 min Bearbietungszeit): Übungsaufgabe zu UML-Sequenzdiagrammen: Der Nachrichtenfluss des MQTT-Protokolls soll anhand eines Beispiels (Pflanzenbewässerung) dargestellt werden._

In einem Büro stehen Zimmerpflanzen, die automatisch bewässert werden sollen. Die Bewässerung und die Pflanzen sollen von außerhalb überwacht werden können. Dafür wird ein WLAN-fähiger Microcontroller verwendet (z.B. ESP32/8266), an den Sensoren und eine Wasserpumpe angeschlossen werden. Für die Überwachung und Fernschaltung der Anlage wird ein Handy mit einer MQTT-App und ein MQTT-Server verwendet. Modellieren Sie im UML-Sequenzdiagramm den folgenden Ablauf unter Nutzung der (englischen) Fachbegriffe von MQTT (anstelle der hier verwendeten allgemeinen Begriffe):

- Die HandyApp bittet den MQTT-Server, zukünftig über Änderungen des Wassertankstands und der Bodenfeuchte der Pflanze informiert zu werden. Die App wartet nicht auf Antwort.

- Der Microcontroller (MCU) bittet den MQTT-Server, künftig über Änderungen des gewünschten Pumpenstatus informiert zu werden. Der MCU wartet nicht auf Antwort.
- Der folgende Ablauf wird zyklisch immer wieder durchgeführt:

  - Nachdem ein Zeitevent eintritt (wir modellieren das wie einen Request von außerhalb des Systems) startet der MCU das Messprogramm und ruft nacheinander die Funktion zum Messen des Wassertankstands  und der Bodenfeuchte der Pflanze ab. Die zugehörigen Werte werden als Antwort auf diesen Request zurückgegeben.

  - Wenn sich die Werte der Bodenfeuchte oder des Wassertanks im Vergleich zur vorigen Messung geändert haben, wird zunächst der neue Bodenfeuchtewert an den MQTT-Server geschickt, dann der Wassertankwert.

  - Wenn in der HandyApp die Pumpe ein- oder ausgeschaltet wird (Trigger von außerhalb, wie das Zeitevent oben), wird der neue Pumpenstatus an den MQTT-Server weitergegeben.

  - Der MQTT-Server sendet an alle, die über Änderungen informiert werden wollten, die neuen Werte.

  - Der MCU schaltet – abhängig vom Empfangenen Pumpenstatus – die Pumpe an oder aus.

#### UML-Sequenzdiagramm

Erstellen Sie zu dem oben genannten Ablauf auf der folgenden Seite ein UML-Sequenz-Diagramm. Eine Notationshilfe finden Sie auf der letzten Seite. (23P)

Wer nach der Bearbeitung das eigene Ergebnis vergleichen will, der kann sich folgende Beispiellösung anschauen:

<button onclick="toggleAnswer('mqtt-sequenz')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="mqtt-sequenz">

![Darstellung der Nachrichtenablaufs eines Pfanzenbewässeungs-MQTT-Systems](plantuml/mqtt-sequenz-uebung.png)

Hinweis zur Lösung:

- 3 Akteure 3P

- 3x Subscripten (async) 3P

- Schleife 2P

- 2x Trigger-events 2P

- 2x Selbstaufrufe 4P

- Alternative 2P

- 3x Publish an MQTT-Server (3P)

- 3x Publish vom MQTT-Server: (3P)

- 1x Schalten 1P 


</span>

#### MQTT-Fachbegriffe

Benennen Sie fachsprachlich, welche MQTT Rollen die oben genannten Systeme (HandyApp, MQTT-Server, MCU) einnehmen. Benennen Sie ggf. den Zeitpunkt, zu dem die jeweilige Rolle eingenommen wird (wenn diese nicht von Anfang an besteht). 

<button onclick="toggleAnswer('mqtt-rollen')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="mqtt-rollen">

HandyApp: Subscriber, Publisher (ab Pumpenclick)

MQTT-Server: Broker

Microcontroller (MCU): Subscriber, Publisher (ab Messung)

</span>

### Weitere Übungsaufgaben

Es gibt hier im Blog noch ein [Tutorial zur Erstellung von UML-Sequenzdiagrammen mit PlantUML](https://oer-informatik.de/uml-sequenzdiagramm-plantuml) sowie eine Reihe Übungsaufgaben zu Sequenzdiagrammen: [Nachrichtenfluss MVP-Pattern](https://oer-informatik.de/uml-sequenzdiagramm-uebung-mvp) / [Authentifizierung mit Passkeys](https://oer-informatik.de/uml-sequenzdiagramm-uebung-passkey) / [Authentifizierung mit OAuth2](https://oer-informatik.de/uml-sequenzdiagramm-uebung-oauth2) sowie [kleinere Übungsaufgaben](https://oer-informatik.de/uml-sequenzdiagramm-uebungen)
