## Aufbau einer Webpage per MVP-Pattern (UML-Sequenzdiagramm Übungsaufgabe)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/113690879145964522</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/uml-sequenzdiagramm-uebung-mvp</span>

> **tl/dr;** _(ca. 20 min Bearbietungszeit): Übungsaufgabe zu UML-Sequenzdiagrammen: Der Nachrichtenfluss des MVP-Patterns soll anhand eines Beispiels (Buchung einer Veranstaltung) dargestellt werden._

Im Rahmen der Organisation eines Kongresses soll ein Teil einer WebApp modelliert werden. Besuchende sollen mit der App Veranstaltungen buchen können. Der hier beschriebene Vorgang startet, nachdem der _Benutzerclient_ bereits die Infoseite einer spezifischen Veranstaltung (im Beispiel die Session-ID `123`) geöffnet hat und über den dort befindlichen "Buchen"-Button einen freien Platz bei der Session buchen will. Die Session nutzt das GUI-Pattern [Model-View-Presenter](https://oer-informatik.de/gui_pattern_mvp), bei dem die Website aus Vorlagendateien (Templates) generiert wird. Die Kenntnis dieses Patterns ist zur Lösung der Aufgabe nicht erforderlich. Modelliere die folgenden Nachrichten in einem UML-Sequenzdiagramm: 

- Der _Benutzerclient_ löst den Versand des _Post-Requests_ `http://it-kongress/buchung {“SessionID“=123, “User“=321}` an den _Presenter_ aus.

- Der _Presenter_ prüft zunächst, ob noch freie Plätze vorhanden sind, indem er die Methode `getFreeSeats(SessionID)` des _SessionRepository_ aufruft (Info abseits der Lösung dieser Aufgabe: das Repository ist Teil der Rolle des Models im MVP-Pattern).

- Für den Fall, dass noch Plätze vorhanden sind, 

    - erzeugt der _Presenter_ eine neue _View_-Instanz über den _View_-Konstruktor `View(“booked.html“)`.

    - Der _Presenter_ bucht einen Platz der Session (`bookSeat(SessionID)` des _SessionRepository_).

- Für den Fall, dass es keine freien Plätze mehr gibt, 

    - erzeugt der _Presenter_ eine andere _View_-Instanz mit `View(“noSeats.html“)`.

    - Der _Presenter_ ruft die statische Methode `sendMailNoSeats(SessionID)` der _WebAppService_-Klasse auf, und wartet nicht bis die Methode die Mail verschickt hat.

- Der _Presenter_ erzeugt die HTML-Antwortseite, in dem er die eigene Methode `htmlText = renderTemplate()` der jeweiligen _View_ aufruft.

- Diese HTML-Antwortseite (`htmlText`) sendet der _Presenter_ als _Response_ (HTTP-Antwort) an den Benutzerclient.

Erstelle ein UML-Sequenzdiagramm, in dem alle geforderten Lebenslinien und Nachrichten dargestellt werden.

### Beispiellösung

Wer nach der Bearbeitung das eigene Ergebnis vergleichen will, der kann sich folgende Beispiellösung anschauen:

<button onclick="toggleAnswer('sessionbuchung')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="sessionbuchung">

![Darstellung der Nachrichtenablaufs beim Aufruf einer Buchungsseite über das MVP-Pattern](plantuml/sessionbuchung.mvp.png)

Hinweis zur Lösung:

- Ich habe zwei verschiedene Views erzeugt, denkbar ist auch, dass die gleiche Lebenslinie erzeugt wird (mein Tool gibt das aber nicht her).

- Die Unterscheidung zwischen den synchronen Nachrichten und der asynchronen Nachricht (Mailversand) müssen umgesetzt worden sein.

- Ebenso sollten Linientyp und die Pfeilspitzen beim Konstruktoraufruf wie angegeben sein.

Meine Bewertung umfasste 15 Punkte::

- 4 Lebenslinien korrekt: 4x 0,5 = 2P

- Benamung der Lebenslinien (":Objekt" bei dreien und "Klasse" bei einem): 2P

- 1 synchrone Nachricht: (ClientRequest und Response): 1P

- 4 Synchrone Nachrichten: 4P

- 2 Erzeugungsnachrichten: 2P

- 1 Asynchrone Nachticht: 2P

- Kombiniertes Fragement (Alt-Bereich): 2P

</span>


### Weitere Übungsaufgaben

Es gibt hier im Blog noch ein [Tutorial zur Erstellung von UML-Sequenzdiagrammen mit PlantUML](https://oer-informatik.de/uml-sequenzdiagramm-plantuml) sowie eine Reihe Übungsaufgaben zu Sequenzdiagrammen: [Nachrichtfluss von MQTT-Projekten](https://oer-informatik.de/uml-sequenzdiagramm-uebung-mqtt)  / [Authentifizierung mit Passkeys](https://oer-informatik.de/uml-sequenzdiagramm-uebung-passkey) / [Authentifizierung mit OAuth2](https://oer-informatik.de/uml-sequenzdiagramm-uebung-oauth2) sowie [kleinere Übungsaufgaben](https://oer-informatik.de/uml-sequenzdiagramm-uebungen)
