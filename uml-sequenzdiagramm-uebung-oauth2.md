## Authentifizieren mit OAuth2 (UML-Sequenzdiagramm Übungsaufgabe)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111555679809480435</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/uml-sequenzdiagramm-uebung-oauth2</span>

> **tl/dr;** _(ca. 10 min Lesezeit): Authentifizierungsverfahren sind von zentraler Bedeutung bei Webservices. In deiser Aufgabe sollen die Akteure und die Nachrichtenfolge von OAuth2 recherchiert werden und schließlich als UML-Sequenzdiagramm dargestellt werden. Auch hier steht neben der Sequenzdiagramm-Übungsaufgabe das Kennenlernen eines zentralen Authentifizierungsmechanismuses im Fokus._


Diese Aufgabe setzt voraus, dass Du die Notationen des [UML-Sequenzdiagramms kennst (Infos z.B. in diesem Blogpost)](https://oer-informatik.de/uml-sequenzdiagramm). 

### Wozu OAuth2?

Bei der Authentifizierung werden in der Regel die Faktoren Besitz, Wissen oder Eigenschaft genutzt, um sicherzustellen, dass die Person, die sich authentifizieren will, diejenige ist, die sie vorgibt zu sein. Das Wissen wird häufig in Form eines geteilten Geheimnisses (Passwort) überprüft. Damit man nicht für jeden Dienst ein eigenes Passwort generieren muss, wurde mit OAuth2 ein Mechanismus geschaffen, der es ermöglicht, sich mit den Zugangsdaten eines bestimmten Dienstes bei anderen Webservices anzumelden.

### Recherchiert die Nachrichtenfolge und die Akteure von OAuth2!

Viele mobile Apps für Webdienste wie GitLab nutzen OAuth2 und Token, um sich beim Webdienst zu authentifizieren. 

Recherchiere den Ablauf einer Authentifizierung zwischen einem User, einem GitLabClient und dem OAuth2-Server. Welche Nachrichten zwischen welchen Kommunikationspartnern (u.a. Passwortübermittlung, Tokenerzeugung, Tokenübergabe) gibt es?

Einstiegspunkte können z.B. die zweiteilige Serie über OAuth2 von Oose sein:

- [Oose: OAuth, OpenID Connect und JWT – wie hängt das alles zusammen? (Teil 1)](https://www.oose.de/blogpost/oauth-openid-connect-und-jwt-wie-haengt-das-alles-zusammen-teil-1/)

- [Oose: OAuth, OpenID Connect und JWT – wie hängt das alles zusammen? (Teil 2)](https://www.oose.de/blogpost/oauth-openid-connect-und-jwt-wie-haengt-das-alles-zusammen-teil-2/)

Oder die Spezifikaiton von OAuth in dem entsprechenden Request for Comments (RFC):

- [The OAuth 2.0 Authorization Framework, RFC6749, Abschnitt 4.1](https://www.rfc-editor.org/rfc/rfc6749#section-4.1)


### Aufgabe

Erstelle für die oben beschriebene Authentifizierung mit OAuth ein UML-Sequenzdiagramm mit den Lebenslinien für alle identifizierten Akteure!

### Beispiellösung

<button onclick="toggleAnswer('oauth2')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="oauth2">

![UML-Sequenzdiagramm für OAuth2](plantuml/oauth-Sequence-diagramm.png)

</span>

### Weitere Übungsaufgaben

Es gibt hier im Blog noch ein [Tutorial zur Erstellung von UML-Sequenzdiagrammen mit PlantUML](https://oer-informatik.de/uml-sequenzdiagramm-plantuml) sowie eine Reihe Übungsaufgaben zu Sequenzdiagrammen: [Nachrichtfluss von MQTT-Projekten](https://oer-informatik.de/uml-sequenzdiagramm-uebung-mqtt) / [Authentifizierung mit Passkeys](https://oer-informatik.de/uml-sequenzdiagramm-uebung-passkey) / [Authentifizierung mit OAuth2](https://oer-informatik.de/uml-sequenzdiagramm-uebung-oauth2) sowie [kleinere Übungsaufgaben](https://oer-informatik.de/uml-sequenzdiagramm-uebungen)

