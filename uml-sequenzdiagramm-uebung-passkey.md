## Authentifizieren mit Passkey (UML-Sequenzdiagramm Übungsaufgabe)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111555679809480435</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/uml-sequenzdiagramm-uebung-passkey</span>

> **tl/dr;** _(ca. 10 min Lesezeit): Das Authentifizierungsverfahren Passkey soll anhand der Beschreibung der Nachrichtenfolge als UML-Sequenzdiagramm dargestellt werden: neben einer Sequenzdiagramm-Übungsaufgabe lernen wir nebenbei den Mechanismus dieses zentralen Authentifizierungsmechanismuses kennen._

Diese Aufgabe setzt voraus, dass Du die Notationen des [UML-Sequenzdiagramms kennst (Infos z.B. in diesem Blogpost)](https://oer-informatik.de/uml-sequenzdiagramm). Dieser Artikel setzt nicht voraus, dass Du die [Passkey Spezifikation der FIDO-Allianz](https://fidoalliance.org/passkeys/) kennst - falls es Dich interessiert ist es aber cleverer, ohne die Aufgabenstellung unten zunächst selbst zu recherchieren, welchen Ablauf diese Authentifizierung hat und daraus ein UML-Sequenzdiagramm zu erstellen.

### Wozu Passkey?

Bei der Authentifizierung werden in der Regel die Faktoren Besitz, Wissen oder Eigenschaft genutzt, um sicherzustellen, dass die Person, die sich authentifizieren will, diejenige ist, die sie vorgibt zu sein.

Häufig wird der Faktor "Wissen" in Form eines Geheimnisses (z.B. Passwort) überprüft, was eingegeben und übermittelt werden muss. Diese Übermittlung ist ein potenzieller Angriffsvektor, den man im Idealfall komplett ausschließen will.

Wird das Passkey-Verfahren angewendet, muss kein Passwort oder anderes Geheimnis übermittelt werden: Ein User, der sich über einen Client (eine WebApp) an einem Server anmelden will, nutzt dazu einen _Authentifikator_ (beispielsweise eine App im Handy). Wie das Verfahren im Einzelnen funktioniert ist in der folgenden Nachrichtenfolge dargestellt:


### Die Nachrichtenfolge ist (vereinfacht) folgendermaßen:

-	Der User teilt dem Client mit, dass er sich per Passkey auf dem Server einloggen will.

-	Der Client bittet den Server, einen zufälligen Hash (eine "Challenge") zu erzeugen und zurückzuschicken.

-	Der Server generiert die Challenge und sendet diese an den Client zurück.

-	Der Client sendet die Challenge an den Authentifikator und bittet diesen, die Challenge zu signieren.

-	Der Authentifikator bittet den User, sich zu authentifizieren (am Handy häufig per Fingerprint oder Gesichtserkennung). Wenn diese Authentifikation erfolgreich ist, signiert der Authentifikator die Challenge mit dem privaten Schlüssel des Passkeys und sendet die signierte Challenge, den Usernamen und eine ID zurück an den Client.

-	Der Client sendet daraufhin die signierte Challenge, den Usernamen und die ID an den Server.

-	Der Server prüft, ob der Benutzer und ID existiert. Wenn er existiert und prüft er mit dem _Public Key_ (wir vereinfachen: diesen kennt der Server bereits), ob die Challenge mit dem _Private Key_ signiert wurde. 

- Wenn die Signatur valide ist, gibt der Server an den Client zurück, dass der User authentifiziert ist. 

- Ist die Signatur nicht valide, schlägt eine der Prüfungen fehl. Der Client erhält eine Fehlermeldung.

### Aufgabe

Erstelle für die oben beschriebene Authentifizierung mit Passkey ein UML-Sequenzdiagramm mit den Lebenslinien für User, Client, Server und Authentifikator.

### Beispiellösung

<button onclick="toggleAnswer('passkey')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="passkey">

![Darstellung der Authentifzierung mit Passkey im UML-Sequenzdiagramm anhand der obigen Nachrichtenfolge](plantuml/passkey.png)

</span>


### Passkeys

Weitere Infos zur Authentifizierung mit Passkeys finden sich in der [Passkey Spezifikation der FIDO-Allianz](https://fidoalliance.org/passkeys/).

### Weitere Übungsaufgaben

Es gibt hier im Blog noch ein [Tutorial zur Erstellung von UML-Sequenzdiagrammen mit PlantUML](https://oer-informatik.de/uml-sequenzdiagramm-plantuml) sowie eine Reihe Übungsaufgaben zu Sequenzdiagrammen: [Nachrichtfluss von MQTT-Projekten](https://oer-informatik.de/uml-sequenzdiagramm-uebung-mqtt)  / [Authentifizierung mit Passkeys](https://oer-informatik.de/uml-sequenzdiagramm-uebung-passkey) / [Authentifizierung mit OAuth2](https://oer-informatik.de/uml-sequenzdiagramm-uebung-oauth2) sowie [kleinere Übungsaufgaben](https://oer-informatik.de/uml-sequenzdiagramm-uebungen)
