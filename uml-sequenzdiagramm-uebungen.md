## UML-Sequenzdiagramm-Übungsaufgaben

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111555679809480435</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/uml-sequenzdiagramm-uebungen</span>

> **tl/dr;** _(ca. 10 min Lesezeit): Kleine Übungsaufgaben mit Lösungen zu UML-Sequenzdiagrammen: TCP Handshake, HTTPS-Fingerprint-Check, Zwei-Faktor-Authentifizierung, Public/Private-Key Verfahren und Challenge-Response Authentication Mechanism (CRAM)._

Diese Aufgaben setzen voraus, dass Du die Notationen des [UML-Sequenzdiagramms kennst (Infos z.B. in diesem Blogpost)](https://oer-informatik.de/uml-sequenzdiagramm). 

### TCP Handshake

Bei einem TCP-Handshake werden eine Reihe von Nachrichten zwischen zwei Hosts gesendet und empfangen. 
Falls Dir die Nachrichtenfolge unten nichts sagt, recherchiere den TCP-Handshake. Stelle die Nachrichten in der richtigen Reihenfolge und mit korrekter Zuordnung von synchronen oder asynchronen Nachrichten korrekt als UML-Sequenzdiagramm dar. Die nötigen Nachrichten sind:

- Verbindungsaufbau per 3-Wege-Handschlag von Host A zu Host B (SYN, SYN_ACK, ACK)

- Exemplarische Datensendung von Host A zu Host B (DATA, ACK)

- Verbindungsabbau von Host A zu Host B (FIN, FIN_ACK, FIN, FIN_ACK)

(Notiere Dir erst Deine Lösung, bevor Du diese Beispiellösung anschaust.)

<button onclick="toggleAnswer('tcphandshake')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="tcphandshake">

Eine beispielhafte Lösung ist die folgende:

![UML-Sequenzdiagram einer TCP-Verbindung](plantuml/tcphandshake.png)

andere Lösungen sind möglich! Diskutiert, was ihr anderes gemacht habt oder nicht versteht!

</span>

### Https-Fingerprint Check

Bei der Kommunikation über _https_ wird die Authentizität des Servers über öffentlich verfügbare _Public Keys_ verifiziert. Die Nachrichtenfolge ist etwa diese:

* Ein HTTPS-Client prüft, ob er den _Public Key_ einer HTTP-Serveraddresse im Cache hat.

* Für den Fall, dass dies nicht der Fall ist, fragt er den _Public Key_ des HTTPS-Servers von einem Keyserver ab.

* Daraufhin holt er den Fingerprint des HTTPS-Servers und verifiziert ihn mit dem _Public Key_.

* Der Client sendet seinen Fingerabdruck wiederum an den HTTPS-Servers zur Prüfung (dieser Teil muss nicht detailliert dargestellt werden).

Stelle diesen Prozess in einem UML-Sequenzdiagramm dar:

<button onclick="toggleAnswer('fingerprint')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="fingerprint">

![HTTPS-Fingerprint-Check als UML Sequenzdiagramm](plantuml/https-fingerprint.png)

</span>

### Zwei-Faktor-Authentifizierung

Viele Anwendungen nutzen Multi-Faktor-Authentifizierung, um die Sicherheit zu erhöhen. Recherchiere die Nachrichtenfolge für die beteiligten Kommunikationspartner einer Dir bekannten 2-Faktor-Authentifizierung und stelle sie als UML-Sequenzdiagramm dar.


<button onclick="toggleAnswer('2fa')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="2fa">

![UML-Sequenzdiagramm für Zwei-Faktor-Authentifizierung](plantuml/2fa.png)

</span>

### Public / Private-Key-Verfahren

Die Authentifizierung an SSH-Servern kann per privat/public-Key-Verfahren erfolgen. Recherchiere die erforderlichen Schritte der Schlüsselerzeugung, Schlüsselübermittlung und anschließender passwortlosen Anmeldung eines SSH-Clients an einem SSH-Server und stelle diese in einem UML-Sequenzdiagramm dar.

(keine Beispiellösung online)

### Challenge–Response Authentication Mechanism (CRAM-MD5)

Recherchiere die Nachrichtenfolge des Challenge-Response-Verfahrens, wie es z.B. bei CRAM-MD5 angewandt wird, und stelle es als UML-Sequenzdiagramm dar.

Eine Erklärung und Auflistung der Nachrichtenfolge findest Du z.B. in dem Dokument [Request for Comments (RFC) 2195](https://datatracker.ietf.org/doc/html/rfc2195).

(keine Beispiellösung online)

### Weitere Übungsaufgaben

* [Authentifizierung mit Passkeys](https://oer-informatik.de/uml-sequenzdiagramm-uebung-passkey) 

* [Authentifizierung mit OAuth2](https://oer-informatik.de/uml-sequenzdiagramm-uebung-oauth2)

* [Nachrichtenfluss beim Erstellen einer Webpage per MVP-Pattern](https://oer-informatik.de/uml-sequenzdiagramm-uebung-mvp)

* [Nachrichten innerhalb eines MQTT-Projekts](https://oer-informatik.de/uml-sequenzdiagramm-uebung-mqtt)
