## UML-Sequenzdiagramme

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/uml-sequenzdiagramm</span>

> **tl/dr;** _(ca. 10 min Lesezeit): Mit UML-Sequenzdiagramme wird die zeitliche Abfolge von Nachrichten zwischen Akteuren dargestellt. Die einzelnen Bestandteile und wesentlichen Notationsmittel werden hier an Beispielen vorgestellt, Literaturtipps und Tooltipps gegeben sowie auf Übungsaufgaben verwiesen._

Dieser Artikel ist Teil einer Artikelreihe über UML-Diagramme: [Klassendiagramm](https://oer-informatik.de/uml-klassendiagramm) / [Sequenzdiagramm](https://oer-informatik.de/uml-sequenzdiagramm) / [Aktivitätsdiagramm](https://oer-informatik.de/uml-aktivitaetsdiagramm) / [Zustandsdiagramm (State)](https://oer-informatik.de/uml-zustandsdiagramm) / [Anwendungsfall-Diagramm (Use Case)](https://oer-informatik.de/uml-usecase)

Es gibt zudem noch ein [Tutorial zur Erstellung von UML-Sequenzdiagrammen mit PlantUML](https://oer-informatik.de/uml-sequenzdiagramm-plantuml) sowie eine Reihe Übungsaufgaben zu Sequenzdiagrammen: [Authentifizierung mit Passkeys](https://oer-informatik.de/uml-sequenzdiagramm-uebung-passkey) / [Authentifizierung mit OAuth2](https://oer-informatik.de/uml-sequenzdiagramm-uebung-oauth2) /  [MQTT-Nachrichtenfluss](https://oer-informatik.de/uml-sequenzdiagramm-uebung-mqtt) sowie [kleinere Übungsaufgaben](https://oer-informatik.de/uml-sequenzdiagramm-uebungen)

 
### Was beschreibt das UML-Sequenzdiagramm?

Mit UML Sequenzdiagrammen werden Nachrichten im zeitlichen Verlauf dargestellt, die zwischen interagierenden Akteuren (z.B. Instanzen von Softwaresystemen) ausgetauscht werden. Sequenzdiagramme sind i.d.R. so angeordnet, dass die vertikale Achse die zeitliche Dimension darstellt, horizontal  sind unterschiedliche Akteure angeordnet.

![Ein einfaches UML-Sequenzdiagramm](plantuml/00_sequenzbeispiel.png)

### Welche Einsatzgebiete haben UML-Sequenzdiagramme?
Sequenzdiagramme können in allen Phasen des Softwareentwicklungsprozesses genutzt. Wichtig ist, dass man den Detailreichtum, die genutzten Spezifikationselemente und die Namensgebung der Elemente immer anpasst der Fragestellung:

> Wem möchte ich mit meinem Sequenzdiagramm was mitteilen?

Adressat und Intention des Diagramms sollten immer im Zentrum stehen und unterscheiden sich je Phase:

- In der Analysephase werden die Nachrichtenflüsse der Geschäftsprozesse dargestellt oder
einzelne Szenarien der Anwendungsfällen präzisiert. Dies geschieht häufig im Dialog mit anderen Projektbeteiligten, dem Auftraggeber, der Fachdomäne. Entsprechend sollten wenig Notationsmittel verwendet werden und die Sprache der Problemdomäne für die Bezeichnungen gewählt werden.

- In der Entwurfs- und Implementierungsphase könnnen Interaktionen der Systemkomponenten, der Nutzer oder Drittsysteme modelliert und dokumentiert werden. Adressaten wären hier v.a. andere Entwickler, daher können mehr Notationselemente verwendet werden und auch die Sprache der Lösungsdomäne verwendet werden.

- Im Rahmen der Qualitätssicherung lassen sich über Sequenzdiagramme Testfälle modellieren, die Schneidung von Integrationstests festlegen und Mocking entscheidungen dokumentierten. Namen sollten bei Akzeptanztests der Problemdomäne, bei Unit- und Integrationstests der Lösungsdomäne entliehen sein.

### Bestandteile eines UML-Sequenzdiagramms

#### Kommunikationspartner

Das Sequenzdiagramm beschreibt Nachrichtenfolgen zwischen Kommunikationspartnern. Die einzelnen Kommunikationspartner werden jeweils an den Kopf eine Lebenslinie gezeichnet: Systeme / Systemkomponenten / Objekte werden als Rechteck gekennzeichnet, vereinzelt werden auch andere Symbole verwendert (z.B. Strichmännchen  für menschliche Akteure).

![Unterschiedliche Kommunikationspartner (Anonyme Objekte, benamte Objekte, menschliche Akteure und Datenbank-Systeme)](plantuml/01_kommunikationspartner.png)

Jeder Kommunikationspartner ist eine identifizierbare Instanz (Objekt) eines Typs (Klasse) und muss deshalb einen Namen tragen. Dieser kann den Instanznamen, den Typnamen oder eine Referenz enthalten.

Der Instanzname ist jeweils mit einem Doppelpunkt vom Typnamen getrennt (``martin: User``). Bei anonymen Instanzen kann der Instanzname weggelassen werden (siehe `` :Backend``), sofern der Typ nicht relevant ist und ein Instanzname vergeben wurde, kann der Typname weggelassen werden (``frontend``). Wenn die Lebenslinie auf das umgebende Objekt referenziert langt ein Hinweis auf ``self``.

Die Nabensgebung von UML-Sequenzdiagrammen folgt vereinfacht folgender Syntax (hier als EBNF notiert):
```ebnf
Lifeline = ([ "<objectname>" ] [":" "<classname>"] ) | "self".
```
Bildhaft als Railroad/Syntaxdiagramm dargestellt entspricht dies:
![](images/Lifeline.gif)

#### Lebenslinien
Von den Bezeichnern der Kommunikationspartner erstreck sich i.d.R. nach unten (entspricht der zeitliche Dimension) die Lebenslinie (_lifeline_).

Sofern die Kommunikationspartner noch passiv sind (keine Nachrichten/Methodenaufrufe bearbeiten) werden die Lebenslinien als einfache oder gestrichelte Linie dargestellt. Im Beispiel unten trifft dies auf Robert Walton zu, der als ich-Erzähler des Romans "Frankenstein" erst spät im Buch eine aktive Rolle übernimmt.

![Lebenslinien im UML-Sequenzdiagramm: aktive und passive Bereiche, Konstruktion und Dekonstruktion](plantuml/02_lebenslinien.png)


Sofern ein Kommunikationspartner etwas ausführt oder auf Fertigstellung eines Methodenaufrufs wartet wird der Bereich als aktiv (Rechteck) gekennzeichnet (wie bei Viktor Frankenstein).
Neue Lebenslinien können durch Konstruktoraufrufe (hier beispielhaft als new() notiert) erzeugt werden. Die jeweiligen Kommunikationspartner sind erst ab diesem Zeitpunkt instanziiert (siehe Unhold, Frankensteins Monster). Die Erzeugungsnachricht wird mit einer gestrichelten Linie und offener Pfeilspitze notiert.

Lebenslinien können ein unbestimmtes Ende haben - die Objektinstanzen gibt es dann weiterhin (siehe Robert, Viktor, Unhold), oder das jeweilige Objekt wird komplett gelöscht (wie bei Henri, der von Unhold umgebracht wird). Dekonstruktionsnachrichten und die Löschung werden durch ein "X" auf der zerstörten Lebenslinie gekennzeichnet.

#### Nachrichtenflüsse

Nachrichten einer Interaktion werden als gerichtete Kante (Pfeil) vom Absender zum Empfänger dargestellt. Linienart und Pfeilspitze definieren hierbei die Art der Nachricht:

Wird eine Nachricht als **synchrone Nachricht** (Aufrufe / _calls_ ) mit ausgefüllter Pfeilspitze notiert, so wartet der Absender auf die Ausführung der Operation. Erst wenn die Aktion beendet ist (und ggf. eine Antwort empfangen wurde) setzt der Absender seine Aktivitäten fort.

![UML-Sequenzdiagramm: Darstellung von Nachrichten, auf deren Ende gewartet wird (synchrone Nachrichten)](plantuml/02_SynchroneNachrichten.png)

Im Gegensatz dazu setzt ein Absender seine weiteren Aktionen unmittelbar nach dem Absenden einer **asynchronen Nachricht** fort. Sie werden mit offener Pfeilspitze ("Indianerpfeil") notiert. Antworten auf asynchrone Nachrichten werden im UML-Sequenzdiagramm als neue unabhängige asynchrone Nachrichten notiert.

![UML-Sequenzdiagramm: Darstellung von Nachrichten, auf deren Ende nicht gewartet wird (asynchrone Nachrichten)](plantuml/03_AsynchroneNachrichten.png)

Sofern der Absender / Empfänger einer Nachricht für die betreffende Modellierung nicht relevant oder unbekannt ist  spricht man von **gefundenen Nachrichten** (_FoundMessage_) bzw. **verlorenen Nachrichten** (_LostMessage)_. Sie starten bzw. enden nicht an Lebenslinien, sondern an einem ausgefüllten Punkt.

![UML-Sequenzdiagramm mit Nachrichten, deren Empfänger oder Absender unbekannt sind (gefundene/verlorene Nachrichten)](plantuml/04_gefundeneVerloreneNachrichtenAllgemein-angepasst.png)

#### Benamung von Nachrichten

Nachrichten können nach folgendem Muster bezeichnet werden (Notation in EBNF):

```
message-name[([parameter-name=]parameter-value, ...)]
```

Bildhaft mit Syntax/Railroad-Diagrammen dargestellt ergeben sich folgende vereinfachte Regelen für Nachrichtennamen:

![EBNF eines Requests als Railroad-Diagramm dargestellt](images/Request.gif)

wobei für die einzelnen Parameter gilt:

![EBNF eines Parameters als Railroad-Diagramm dargestellt](images/Parameter.gif)

Das heißt:

- ein Name _muss_ genannt werden,

- Argumente _können_ benannt werden,

- Die Bezeichner der übergebenen Argumente (Parameternamen) _können_ genannt werden.

Im Fall einer Antwortnachricht kann der Rückgabewert angegeben werden sowie die Variable, in der dieser Rückgabewert gespeichert wurde:

```
[variable = ] message-name [([parameter-name=]parameter-wert, ...)] [ : returnvalue]
```

![EBNF einer Antwort als Railroad-Diagramm dargestellt](images/Antwort.gif)

#### Selbstnachrichten

Nachrichten kann jeder Teilnehmer auch an sich selbst schicken (Methodenaufrufe eines Objekts auf sich selbst). Absenderin und Empfängerin sind dann identisch.

![UML-Diagramm mit Selbstsaufrufen](plantuml/07_Selbstaufruf.png)

### Kombinierte Fragmente zur Darstellung des Kontrollflusses

#### Schleifen im Sequenzdiagramm

Schleifen werden im UML-Sequenzdiagramm über kombinierte Fragmente dargestellt. Im Fall einer Wiederholung über das Schlüsselwort `loop` in Kombination mit einem _guard_ (im einfachsten Fall der Anzahl der Schleifendurchläufe oder einer Bedingung, bis zu deren zutreffen der Schleifenrumpf ausgeführt wird).

![Darstellung einer Schleife im UML-Sequenzdiagramm durch ein kombiniertes Fragment](plantuml/08_Wiederholungsstruktur.png)

#### Bedingte Anweisungen im Sequenzdiagramm

Auch bedingte Anweisungen werden als kombiniertes Fragment dargestellt. Der _guard_ (die Bedingung) wird mit eckigen Klammern umgeben:

![Darstellung einer bedingten Anweisung im UML-Sequenzdiagramm durch ein kombiniertes Fragment](plantuml/09_BedingteAnweisung.png)

#### Concurrency - Parallelisierung im Sequenzdiagramm

Wenn mehrere Interaktionen parallel verlaufen wird dies mit dem kombinierten Fragment `par` notiert. Die Abläufe der einzelnen Bereiche können somit zeitgleich verlaufen. Die Nachrichten ausserhalb des kombinierten Fragments erfolgen wieder synchronisiert und sequenziell.

![Darstellung von Parallelisierung und Synchronisierung im UML-Sequenzdiagramm durch ein kombiniertes Fragment](plantuml/10_Parallelitaet.png)

#### Weitere kombinierte Fragmente

Analog zu den vorgenannten kombinierten Fragementen `loop`, `alt` und `par` gibt es eine Reihe weiterer Bezeichner, die in den Rahmenköpfen notiert werden und für folgende Eigenschaften stehen:

- `opt`: Nachrichten in diesem Rahmen sind optional

- `strict`: die modellierte Nachrichtenreihenfolge muss zwingend erhalten beleiben

- `critical`: Die Nachrichtenfolge darf auf keinen Fall unterbrochen werden und gilt als unteilbar (atomar).

### Referenzen: Auslagern von Abschnitten in weitere Sequenzdiagramme

Zur Übersichtlichkeit können Sequenzdiagrammme aufgeteilt und ineinander referenziert werden. Beispielsweise soll in folgendem Diagramm die Zustandsänderung des _Subject_ und Benachrichtigung des _ConcreteObserver_ ausgelagert werden von dem Triggern durch den Client:

![Referenzen innerhalb eines UML-Sequenzdiagramms (mit Inhalt)](plantuml/13_ReferenzAllesInEinem.png)

Hierzu wird an Stelle der auszulagernden Nachrichten ein Referenzrahmen eingefügt (mit dem UML-Schlüsselwort `ref` und einer Bezeichnung des ausgelagerten UML-Sequenzdiagramms:)

![Referenzen innerhalb eines UML-Sequenzdiagramms (ohne Inhalt)](plantuml/13_Referenz.png)

Der referenzierte Bereich wird dann an anderer Stelle in einem Interaktionsrahmen als eigenes Diagramm angegeben. Der UML-Standard erwartet, dass dem Namen des Interaktionsrahmens  `sd` (für _sequence diagram_) vorangestellt wird. Beispiel:

![Referenziertes UML-Sequenzdiagramm](plantuml/13_ReferenzierterInteraktionsrahmen.png)

### Zeitliche Zusicherungen

Sofern die Abfolge von Nachrichten an bestimmte zeitliche Zusicherungen gebunden ist, können dies im Diagramm angegeben werden.

Zeitdauern zwischen Nachrichten werden über einen Doppelpfeil mit angegebenem _guard_, der nach UML-Notation in geschweiften Klammer stehen muss, angegeben (_DurationConstraint_).

![Beispiele der zeitlichen Zusicherungen im UML Sequenzdiagramm](plantuml/14_ZeitlicheZusicherungen.png)

Darüber hinaus können auch direkt an einzelnen Nachrichten zeitliche Festlegungen getroffen werden:

- _TimeObservation_: es werden bestimmte Zeitpunkte definiert, z.B. wird der Zeitpunkt, zu dem eine Nachricht abgesendet wird mit `t=now` direkt neben der abgehenden Nachricht an der Lebenslinie notiert.

- _TimeConstraint_: Zeitpunkte, an denen eine Nachricht eintrifft oder abgesendet werden soll können definiert werden - wie alle Constraints ist diese in geschweiften Klammern notiert. Wenn eine Nachricht 3 Sekunden nach dem oben festgelegten Zeitpunkt `t` abgesendet werden soll notiert man beispielsweise `t+3s`

### Weitere Literatur zu UML-Sequenzdiagrammen

- **als Primärquelle**: die UML Spezifikation der Object Management Group
Definition des Standards, jedoch nicht für Endnutzer aufbereitet): [https://www.omg.org/spec/UML](https://www.omg.org/spec/UML)

- **für den Einstieg**: Martina Siedl, Marion Brandsteidl, Christian Huemer, Gerti Kappel: UML@Classroom, dpunkt Verlag, Heidelberg 2012
Gut zu lesende Einführung in die wichtigsten UML-Diagramme, Empfehlung für den Einstieg.

- **für Lesefaule**: Die Vorlesungsreihe der Technischen Uni Wien (zu UML@Classroom) kann hier angeschaut werden (Videos, Folien): [http://www.uml.ac.at/de/lernen](http://www.uml.ac.at/de/lernen)

- **als Nachschlagewerk**: Christoph Kecher, Alexander Salvanos: UML 2.5 – Das umfassende Handbuch, Rheinwerk Bonn 2015, ISBN 978-3-8362-2977-7
Sehr umfangreiches Nachschlagewerk zu allen UML-Diagrammen

- **als Cheatsheet**: die Notationsübersicht von oose.de: [https://www.oose.de/wp-content/uploads/2012/05/UML-Notations%C3%BCbersicht-2.5.pdf](https://www.oose.de/wp-content/uploads/2012/05/UML-Notations%C3%BCbersicht-2.5.pdf)

- **UML und Software Engineering**: Chris Rupp, Stefan Queins & die SOPHISTen: UML2 glasklar, Hanser Verlag, München 2012; ISBN 978-3-446-43057-0 Schwerpunkt: Einbindung von UML-Diagrammen in den Softwareentwicklungszyklus

- **Zur Zertifizierungs-Vorbereitung**: M. Chonoles: OCUP 2 Certification Guide , Morgan Kaufmann Verlag, Cambridge 2018, ISBN 978-0-12-809640-6
    Informationen zur Zertifizierung nach OMG Certified UML Professional 2™ (OCUP 2™): Foundation Level

### Software zur Erzeugung von UML-Sequenzdiagrammen

- [PlantUML](http://www.plantuml.com): Deklaratives UML-Tool, das in vielen Entwicklungsumgebungen integriert ist. auch als WebEditor verfügbar. Die obigen Diagramme wurden damit erzeugt - [Eine detaillierte Anleitung für plantUml mit den Quelltexten findet sich hier](https://oer-informatik.gitlab.io/uml/umlsequenz/uml-sequenzdiagramm-plantuml.html)

- [WhiteStarUML](https://sourceforge.net/projects/whitestaruml/) (Relativ umfangreiches Tool, viele UML-Diagramme, mit Code-Generierung für Java, C, C# und Vorlagen für Entwurfsmuster)

- [Draw.io](http://www.draw.io): Online-Tool für FLowcharts usw - aber eben auch UML

### Übungsaufgaben zu UML-Sequenzdiagrammen:

- [Authentifizierung mit Passkeys](https://oer-informatik.de/uml-sequenzdiagramm-uebung-passkey)

- [Authentifizierung mit OAuth2](https://oer-informatik.de/uml-sequenzdiagramm-uebung-oauth2)
